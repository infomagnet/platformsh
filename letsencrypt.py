#!/usr/bin/python
import argparse
import os
import subprocess
import sys

parser = argparse.ArgumentParser(description='Get a letsencrypt certificate and update platform.sh domains.')

parser.add_argument('-m', '--maindomain', nargs=1, help='Main domain of the certificate', required=True)
parser.add_argument('-e', '--email', nargs='?', help='Email that request the certificate', required=True)
parser.add_argument('-d', '--domains', nargs='+', help='Platform.sh domains to update', required=True)
parser.add_argument('-x', '--extra', nargs='*', help='Platform.sh extra domainst to add to the certificate')
parser.add_argument('cmd', nargs=1, help='run/renew')

args = parser.parse_args()

def unique(seq):
  seen = set()
  seen_add = seen.add
  return [x for x in seq if not (x in seen or seen_add(x))]

domains = args.maindomain + args.domains
if args.extra:
  domains += args.extra

domains = unique(domains)

# Get certificate
cmd = 'lego --email=' + args.email + ' ' + ' '.join([ '--domains ' + s for s in domains]) + ' --webroot=' + os.environ['PLATFORM_DOCUMENT_ROOT'] + ' --path=' + os.environ['PLATFORM_APP_DIR'] + '/keys/' + ' -a ' + args.cmd[0]
subprocess.call(cmd, shell=True)

# Split certificate for platform.sh format
certbasename = os.environ['PLATFORM_APP_DIR'] + '/keys/certificates/' + args.maindomain[0]
cmd = 'csplit -f ' + certbasename + '.crt- ' + certbasename + '.crt ' + "'/-----BEGIN CERTIFICATE-----/' '{1}' -z -s"
subprocess.call(cmd, shell=True)

# Update the certificates on the domain
for domain in args.domains:
  cmd = 'platform domain:update -p ' + os.environ['PLATFORM_PROJECT'] + ' --no-wait --yes --cert ' + certbasename + '.crt-00 --chain ' + certbasename + '.crt-01 --key ' + certbasename +'.key ' + domain
  subprocess.call(cmd, shell=True)

#!/usr/bin/env bash
set -e

DOMAIN=$2
EMAIL=$3
CMD=$1

if [ $# -eq 0 ] || [ ! "$#" -ge 3 ]
  then
    echo -e "\nArguments missing!\n\nIt's ./letsencrypt.sh [run|renew] [naked-domain] [email-account] (subdomain) (run/renew - default to renew if empty)"
    exit 1
fi

if [ -z "$4" ]
  then
    CERTIFICATE_DOMAIN=$DOMAIN
  else
    CERTIFICATE_DOMAIN=$4.$DOMAIN
fi

if [ "$PLATFORM_BRANCH" = "master" ]
  then
    # Renew/$CMD the certificate
    lego --email=$EMAIL --domains=$CERTIFICATE_DOMAIN --webroot=$PLATFORM_DOCUMENT_ROOT --path=$PLATFORM_APP_DIR/keys/ -a $CMD
    # Split the certificate from any intermediate chain
    csplit -f $PLATFORM_APP_DIR/keys/certificates/$CERTIFICATE_DOMAIN.crt- /app/keys/certificates/$CERTIFICATE_DOMAIN.crt '/-----BEGIN CERTIFICATE-----/' '{1}' -z -s
    # Update the certificates on the domain
    platform domain:update -p $PLATFORM_PROJECT --no-wait --yes --cert $PLATFORM_APP_DIR/keys/certificates/$CERTIFICATE_DOMAIN.crt-00 --chain $PLATFORM_APP_DIR/keys/certificates/$CERTIFICATE_DOMAIN.crt-01 --key $PLATFORM_APP_DIR/keys/certificates/$CERTIFICATE_DOMAIN.key $DOMAIN
fi
